window.FontAwesomeConfig = {
    searchPseudoElements: true
};
$(document).ready(function() {
    $('.navbar__list').hide();
    $('.navbar__button').click(function() {
       $(this).toggleClass('navbar__button--active');
       $('.navbar__list').slideToggle();
    });
    $(document).mouseup(function(e) {
        if(!$('.navbar__button').is(e.target) && $('.navbar__button--active').length !== 0 ) {
            $('.navbar__button').toggleClass('navbar__button--active');
            $('.navbar__list').slideUp();
        }
    });
});