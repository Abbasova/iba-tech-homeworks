function enter() {
    numb1 = parseInt(prompt("Enter first number: "));
    while(isNaN(numb1)) {
        numb1 = parseInt(prompt("Enter first number again: "));
    }
    numb2 = parseInt(prompt("Enter second number: "));
    while(isNaN(numb2)) {
        numb2 = parseInt(prompt("Enter second number again: "));
    }
    operation = prompt("Enter your operation: ");
    while(operation!='+' && operation!='-' && operation!='*' && operation!='/') {
        operation = prompt("Enter operation correctly: ");
    }
}
function calc() {
    enter();
    switch (operation) {
        case '+':
            return numb1 + numb2;
        case '-':
            return numb1 - numb2;
        case '*':
            return numb1 * numb2;
        case '/':
            return numb1 / numb2;
    }
}
let numb1, numb2, operation;
console.log(calc());
