const confirm = document.querySelector('.btn');
const password1 = document.querySelector('.password1');
const password2 = document.querySelector('.password2');
const eventHandlerForIcons = (event) => {
    if(event.target.classList.contains("fa-eye-slash")) {
        event.target.parentNode.querySelector('input').type = 'text';
    }else if(event.target.classList.contains("fa-eye")) {
        event.target.type = 'password';
        event.target.parentNode.querySelector('input').type = 'password';
    }
    event.target.classList.toggle("fa-eye-slash");
    event.target.classList.toggle("fa-eye");
};
const icons = document.querySelectorAll('.icon-password');
for (let icon of icons) {
    icon.addEventListener('click', eventHandlerForIcons);
}
confirm.addEventListener('click', () => {
    if (password1.value === password2.value) {
        alert('You are welcome');
    }else {
        const message = document.querySelector('.message');
        if (!message) {
            const message = document.createElement('p');
            message.classList.add('message');
            message.textContent = 'You need to enter the identical values';
            message.style.color = 'red';
            password2.after(message);
        }
    }
});
const eventHandlerforMessage = () => {
    const message = document.querySelector('.message');
    if (message) {
        message.remove();
    }
};
password1.addEventListener('focus', eventHandlerforMessage);
password2.addEventListener('focus', eventHandlerforMessage);