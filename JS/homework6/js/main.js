const filterBy = (dataArray, dataType) => {
    return dataArray.filter(item => typeof(item) !== dataType);
};
const filterByWithoutArrayMethods = (dataArray, dataType) => {
    let i = 0;
    let newArray = [];
    for(let item of dataArray) {
        if (typeof(item) !== dataType) {
            newArray[i] = item;
            i++;
        }
    }
    return newArray;
};
const arr = ["HTML", 12, "-33", -23, null, 24.6, true, "Javascript", 54, false, 766, undefined , 0, 435];
const filteredArray1 = filterBy(arr, 'string');
const filteredArray2 = filterByWithoutArrayMethods(arr, 'string');
filteredArray1.forEach((item) => {
    console.log(item);
});
for(let item of filteredArray2) {
    console.log(item);
}