$(document).ready(function () {
    $('.tabs-content li').hide();
    $(`.${ $('.active').attr('id') }`).fadeIn();
    $('.tabs-title').click(function () {
        $('.active').removeClass('active');
        $(this).addClass('active');
        $('.tabs-content li').hide();
        $(`.${ $('.active').attr('id') }`).fadeIn();
    });
});
