## Theoretical question

<p>1. Explain in your own words the difference between declaring variables via `var`, `let` and `const`. 
<p>One of the features that came with ES6 is addition of let and const which can be used for variable declaration. `var` is function scope, `let` and `const` is block scope.
We can access variable which declare by `var`, from outside from block, but we can not access if it declare via `let`.
We can change value of variable declaring via `var` and `let`, however we can not change `const` value.</p>

<p>2. Why is declaration of a variable via `var` considered a bad tone?</p>
<p>When we want to use EcmaScript6 we have to use `let` instead of `var`. `var` is for old versions.</p>