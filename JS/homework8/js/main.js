const priceInput = document.querySelector('.priceInput');
const readValue = () => parseInt(priceInput.value);
priceInput.addEventListener ('focus', (event) => {
    event.target.style.border = "2px solid green";
});
priceInput.addEventListener ('blur', (event) => {
    if (readValue() > 0 || readValue() === 0 ) {
        if ( document.querySelector('.wrongPrice')) {
            document.querySelector('.wrongPrice').remove();
        }
        event.target.style.border = "";
        event.target.style.color = "green";
        if ( document.querySelector('.currentPrice')) {
            const span = document.querySelector('span');
            span.textContent = `Current price: ${readValue()}`;
        }else {
            const span = document.createElement('span');
            span.classList.add("currentPrice");
            span.style.lineHeight = "4";
            span.textContent = `Current price: ${readValue()}`;
            const form = document.querySelector('form');
            form.before(span);
            const btn = document.createElement('button');
            btn.classList.add("btn");
            btn.textContent = "x";
            btn.style.marginLeft = "30px";
            span.after(btn);
            btn.addEventListener( 'click', (event) => {
                priceInput.value = 0;
                if ( document.querySelector('.currentPrice')) {
                    const span = document.querySelector('span');
                    span.textContent = `Current price: ${readValue()}`;
                }
            });
        }
    }else {
        if ( document.querySelector('.currentPrice')) {
            document.querySelector('.currentPrice').remove();
            document.querySelector('.btn').remove();
        }
        if(document.querySelector('.wrongPrice')) {
        }else {
            event.target.style.border = "2px solid red";
            const span = document.createElement('span');
            span.classList.add("wrongPrice");
            span.style.lineHeight = "4";
            span.textContent = "Please enter correct price";
            const form = document.querySelector('form');
            form.after(span);
        }
    }
});
