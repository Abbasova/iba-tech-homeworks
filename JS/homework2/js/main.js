let number = parseInt(prompt("Enter any number: "));
while(isNaN(number)) {
    number = parseInt(prompt("Enter the number again: "));
}
let i, count = 0;
for(i=0; i<=number; i++) {
    if(i%5 === 0 && i!==0) {
        console.log(i);
        count++;
    }
}
if(count === 0) console.log('Sorry, no numbers');
// --------------------------------------------------
let m = parseInt(prompt("Enter first number: "));
let n = parseInt(prompt("Enter second number: "));
while(isNaN(m) || isNaN(n)) {
    m = parseInt(prompt("Enter first number again: "));
    n = parseInt(prompt("Enter second number again: "));
}
for(m; m<=n; m++) {
    let i;
    let x=0;
    for (i=1; i <= m/2; i++) {
        if(m%i === 0) {
            x++;
        }
    }
    if(x === 1) {
        console.log(m);
    }
}
