const images = document.querySelectorAll('.image-to-show');
const timer = document.querySelector('.timer');
for( let i=1; i<images.length; i++) {
    images[i].style.display = "none";
    images[i].style.opacity = 0;
}
let i=0;
const slider = (second) => {
    let opacity = 0;
    let opacityInterval = setInterval(function () {
        images[i].style.display = "block";
        if(i===0) {
            images[ images.length - 1 ].style.opacity = 0;
            images[ images.length - 1 ].style.display = "none";
        }else {
            images[ i - 1 ].style.opacity = 0;
            images[ i - 1 ].style.display = "none";
        }
        if (opacity >= 0.9){
            clearInterval(opacityInterval);
        }
        images[i].style.opacity = opacity;
        opacity += 0.1;
    }, (second / 1 * 0.1 * 1000));
    i++;
    if(i===images.length) i=0;
}
let intervalId = setInterval( slider, 10000, 0.5);
const stopBtn = document.querySelector('.stop');
const resumeBtn = document.querySelector('.resume');
stopBtn.addEventListener('click', () => {
    clearInterval(intervalId);
    intervalId = false;
    clearInterval(timerNextImg);
    timerNextImg = false;
});
resumeBtn.addEventListener('click', () => {
    if(!intervalId) intervalId = setInterval( slider, 10000, 0.5);
    if(! timerNextImg) timerNextImg = setInterval( () => {
        if(s===1 || s===2) timer.textContent = `There is ${--s} second to next picture.`;
        else timer.textContent = `There are ${--s} seconds to next picture.`;
        if(!s) s=10;
    }, 1000);
});
let s=10;
let timerNextImg = setInterval( () => {
    if(s===1 || s===2) timer.textContent = `There is ${--s} second to next picture.`;
    else timer.textContent = `There are ${--s} seconds to next picture.`;
    if(!s) s=10;
}, 1000);