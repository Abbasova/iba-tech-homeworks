## Theoretical questions:

<p>1. Describe in your own words the difference between the `setTimeout()` and `setInterval()` functions.</p>
<p>setTimeout runs the function once certain time later. setInterval runs the function not only once, but regularly after the given interval of time.</p>
<p>2. What happens if a zero delay will be passed to the `setTimeout()` function? Will it work instantly, and why</p>
<p>This schedules the execution of function as soon as possible, only after the current code is complete.</p>
<p>3. Why is it important to remember to call the `clearInterval()` function when you don't need a previously created launch cycle anymore?</p>
<p>If we do not clear it will work like infinite loop.</p>

