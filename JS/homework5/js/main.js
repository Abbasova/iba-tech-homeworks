const createNewUser = (name, surname, birthday) => {
    const newUser = {
        firstName: name,
        lastName: surname,
        birthday: birthday,
        getLogin: function () {
            return (this.firstName + this.lastName).toLowerCase();
        },
        setFirstName: function (newname) {
            this.firstName = newname;
        },
        setLastName: function (newsurname) {
            this.lastName = newsurname;
        },
        getAge: function () {
            const birthdayArray = this.birthday.split(".");
            for (let i = 0; i < birthdayArray.length; i++) {
                birthdayArray[i] = parseInt(birthdayArray[i]);
            }
            const now = new Date();
            let age = now.getFullYear() - birthdayArray[2];
            if( now.getMonth() - birthdayArray[1] + 1 < 0 ) age--;
            else if( now.getMonth() - birthdayArray[1] + 1 === 0 ) {
                if(now.getDate() - birthdayArray[0] < 0 ) age--;
            }
            return age;
        },
        getPassword: function () {
            const birthdayArray = this.birthday.split(".");
            let password = this.firstName + this.lastName + birthdayArray[2];
            password = password.toLowerCase();
            password = password.charAt(0).toUpperCase() + password.substring(1);
            return password;
        }
    };
    return newUser;
};
const user = createNewUser("Fidan", "Abbasova", "05.09.1996");
console.log(`Username: ${user.getLogin()}`);
console.log(`Password: ${user.getPassword()}`);
console.log(`Age: ${user.getAge()}`);

