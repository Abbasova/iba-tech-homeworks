const tabs = document.querySelectorAll('.tabs-title');
const listItems = document.querySelectorAll('.tabs-content li');
for ( let i=0; i<tabs.length; i++) {
    if (!tabs[i].classList.contains('active')) {
        listItems[i].hidden = true;
    }
}
for (let tab of tabs) {
    tab.addEventListener('click', (event) => {
        for (let tab of tabs) {
            tab.classList.remove('active');
        }
        event.target.classList.add('active');
        for ( listItem of listItems) {
            listItem.hidden = true;
        }
        const id =  event.target.id;
        document.querySelector(`.${id}`).hidden = false;
    });
}

