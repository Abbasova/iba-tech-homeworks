const menus = ['home', 'posts', 'buildings', 'magazine', 'news'];
console.log(menus);
$(document).ready(function () {
    $('.navbar-list-item').on( 'click', function(e) {
        $('.navbar-list-item').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
        let $id = $(this).attr('href');
        $('html, body').animate({
            scrollTop: $($id).offset().top - 70
        }, 700);
    });
    $('.scroll-up').on('click', function (e) {
       $('html, body').animate({
           scrollTop: 0
       }, 1000);
    });
    let $hidden = true;
    $('.news-grid').hide();
    $('.toggle-btn').on('click', function () {
        $hidden = !$hidden;
       $('.news-grid').slideToggle();
       if(!$hidden) $(this).text('Show less');
       else $(this).text('Show more');
    });
});
$(window).scroll(function () {
    const $top = $(window).scrollTop();
    if($top > $(window).height()){
        $('.scroll-up').fadeIn();
    } else {
        $('.scroll-up').fadeOut();
    }
    menus.forEach(function (menuItem) {
        if($top >= $(`#${menuItem}`).offset().top - 70 && $top < $(`#${menuItem}`).offset().top + $(`#${menuItem}`).height() - 70 ) {
            $('.navbar-list-item').removeClass('active');
            $(`.navbar-list-item[href="#${menuItem}"]`).addClass('active');
        }
    });
});