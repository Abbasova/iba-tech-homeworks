const createNewUser = (name, surname) => {
    const newUser = {
        firstName: name,
        lastName: surname,
        getLogin: function () {
            return (this.firstName.charAt(0) + this.lastName).toLowerCase();
        },
        setFirstName: function (newname) {
            this.firstName = newname;
        },
        setLastName: function (newsurname) {
            this.lastName = newsurname;
        }
    };
    return newUser;
};
const user = createNewUser("Fidan", "Abbasova");
console.log(user.getLogin());
user.setFirstName("Ivan");
console.log(user.getLogin());
user.setLastName("Vasilivich");
console.log(user.getLogin());
