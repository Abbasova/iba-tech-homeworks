// const btns = document.querySelectorAll('.btn-wrapper .btn');
// document.addEventListener('keydown', (event) => {
//     for (let btn of btns) {
//         if (btn.classList.contains(event.key)) {
//             for (let btn of btns) {
//                 btn.style.backgroundColor = "black";
//             }
//             btn.style.backgroundColor = "blue";
//         }
//     }
// });
const btns = document.querySelectorAll('.btn-wrapper .btn');
document.addEventListener('keydown', (event) => {
    for (let btn of btns) {
        if (btn.classList.contains(event.key.toLowerCase())) {
            for (let btn of btns) {
                btn.classList.remove('active');
            }
            btn.classList.add('active');
        }
    }
});