const buttonSwitch = document.querySelector('.theme-switch');
const header = document.querySelector('.header');
const navbar = document.querySelector('.navbar');
const footer = document.querySelector('.footer');
const buttons = document.querySelectorAll('.header .header-buttons-row .header-button');
const titles = document.querySelectorAll('.section-title');
const line = document.querySelector('.clients .clients-grid .clients-title .line');
const clientSection = document.querySelector('.clients');
const setTheme = () => {
    let removedColor;
    if( localStorage.getItem('theme') === null ) localStorage.setItem('theme', 'violet');
    color = localStorage.getItem('theme');
    if( color === 'grey') {
        removedColor = 'violet';
    }
    if( color === 'violet') {
        removedColor = 'grey';
    }
    buttonSwitch.classList.add(`${removedColor}-bg`);
    header.classList.add(`transparent-${color}-bg`);
    navbar.classList.add(`${color}-bg`);
    footer.classList.add(`${color}-bg`);
    buttons.forEach(btn => {
        btn.classList.add(`${color}-bg`);
    });
    titles.forEach(title => {
        title.classList.add(`${color}-text`);
    });
    line.classList.add(`${color}-bg`);
    clientSection.classList.add(`transparent-${color}-bg`);
    
    buttonSwitch.classList.remove(`${color}-bg`);
    header.classList.remove(`transparent-${removedColor}-bg`);
    navbar.classList.remove(`${removedColor}-bg`);
    footer.classList.remove(`${removedColor}-bg`);
    buttons.forEach(btn => {
        btn.classList.remove(`${removedColor}-bg`);
    });
    titles.forEach(title => {
        title.classList.remove(`${removedColor}-text`);
    });
    line.classList.remove(`${removedColor}-bg`);
    clientSection.classList.remove(`transparent-${removedColor}-bg`);
}
setTheme();
buttonSwitch.addEventListener('click', () => {
    if(localStorage.getItem('theme') === 'grey' ) localStorage.setItem('theme', 'violet'); 
    else if(localStorage.getItem('theme') === 'violet' ) localStorage.setItem('theme', 'grey'); 
    setTheme();
});
